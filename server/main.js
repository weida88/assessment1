const express = require("express");
const app = express();
const port = 3000;

app.use(express.static(__dirname + "/../client"));
app.use(express.static(__dirname + "/../bower_components"));

app.listen(port, function(){
    console.info("Application started on port %d", port);
});

app.post('/submitted', function(req, res) {
    res.send('form submitted!');
});
