(function(){

    var FormApp = angular.module("FormApp", []);

    var FormCtrl = function(){

        var formCtrl = this;

        formCtrl.detail={
            email: "",
            password: "",
            name: "",
            gender: "",
            dateofbirth: "",
            address: "",
            country: "",
            contact: ""
        };


        formCtrl.details=[];

        formCtrl.submitform = function(){

            formCtrl.details.push(formCtrl.detail);
            console.log(formCtrl.details);
            formCtrl.detail="";
        }
    };

    FormApp.controller("FormCtrl", [FormCtrl]);

})();
